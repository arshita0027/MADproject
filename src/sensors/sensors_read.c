#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<shunyaInterfaces.h>
#include<time.h> 
#include<si/shunyaInterfaces.h>

struct sensors
{
    int16_t accelerometer_x;
    int16_t accelerometer_y;
    int16_t accelerometer_z;
    int16_t temperature;
    int sound_level;
    time_t seconds;
};

const int MPU6050_addr = 0x68;  // I2C address of the MPU-6050. If AD0 pin is set to HIGH, the I2C address will be 0x69.

struct sensors scan(void);
void printSensors(struct sensors input);

struct sensors values;

int main()
{
    int16_t accelerometer_x, accelerometer_y, accelerometer_z, temperature;
    wireBegin(1);
    wireBeginTransmission(MPU6050_addr);  // Begins a transmission to the I2C slave (GY-521 board)
    wireWrite(0x6B);
    wireWrite(0);   // set to zero (wakes up the MPU-6050)
    wireEndTransmission(1);

    int sound_level;
    initLib();

    time_t seconds; 

    printSensors(scan());

    return 0;
}

struct sensors scan(void)
{
    wireBeginTransmission(MPU6050_addr);
    wireWrite(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
    wireEndTransmission(0);   // the parameter indicates that the Arduino will send a restart. As a result, the connection is kept active.
    wireRequestFrom(MPU6050_addr,14);   // request a total of 7*2=14 registers

    // "wireRead()<<8|wireRead();" means two registers are read and stored in the same variable
    values.accelerometer_x = wireRead()<<8|wireRead();
    values.accelerometer_y = wireRead()<<8|wireRead();
    values.accelerometer_z = wireRead()<<8|wireRead();
    values.temperature = wireRead()<<8|wireRead();

    values.sound_level = getAdc(1);
 
    time(&values.seconds);   // Stores time seconds
    
    return values;
}

void printSensors(struct sensors input)
{
    printf("Acceleration-X : %d\n",input.accelerometer_x);
    delay(100);
    printf("Acceleration-Y : %d\n",input.accelerometer_y);
    delay(100);
    printf("Acceleration-Z : %d\n",input.accelerometer_z);
    delay(100);
    printf("Temperature : %d\n",input.temperature);
    delay(100);  
    printf("Sound = %d\n", input.sound_level);
    delay(1000);
    printf("Seconds since January 1, 1970 = %ld\n", input.seconds);  
}

