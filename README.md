# Motor Anomaly Detection
## Introduction
Motor are the backbone of the modern industry, and no one likes unexpected maintenance.
We monitor various motor parameters and alerts the user in case of an anomaly.

## ShunyaOS and ShunyaInterfaces
ShunyaOS is a lightweight Linux arm Operating system with built-in support for AI and IOT.  
ShunyaInterfaces is a set of API that allows you to rapidly create and deploy AIoT products with ease.  
For more information on ShunyaOS : demo.shunyaos.org

## Contributing to MADproject 
MAD Welcomes you to contribute to help make this project.
Ways you can help:
* Choose from the existing issue and work on those
* Feel like MAD could use a new feature, make an issue to discuss how it can be implemented and work on it
* Find a bug and report it.
* Help fix a reported bug.
* Help with documenting the whole project.
* Write a Howto Guide.

How to work on an issue: 
* clone the repo 
* put in a comment saying you are doing this task and estimated time needed
* make a branch, give branch name same as the issue title you are working on
* do the task (writing code, Documentation, Research)
* Test code in ShunyaOS docker file [Available here](https://hub.docker.com/r/shunyaos/shunya-armv7)
* make a Merge request
* Maintainer reviews the code and merges 
* start working on another issue.
## Architecture 
![Architecture](./extras/architecture.png)
## Current maintainer
Asim hasssan (@asimhassan939)
## Active contributers.
* Afshur Thoufeeq Musharraf K - [@afshur_thoufeeq_musharraf_k ](https://gitlab.com/afshur_thoufeeq_musharraf_k)
* Akshay Panchal - [@akshaykpanchal ](https://gitlab.com/akshaykpanchal)
* Prince Bhalawat - [@Prince27](https://gitlab.com/Prince27)
* Mayuri Pandey - [@mpmayuripandey12](https://gitlab.com/mpmayuripandey12)